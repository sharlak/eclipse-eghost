package es.deusto.eghost.example7.popup.actions;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

import filesystem.Filesystem;
import filesystem.templates.Filesystem2Script;

public class GenerateAction implements IObjectActionDelegate {

	private Shell shell;
	private IStructuredSelection selection;

	/**
	 * Constructor for Action1.
	 */
	public GenerateAction() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		Filesystem filesystem = null;
		Object obj = this.selection.getFirstElement();
		// TODO: Comprobar si el fichero está guardado y la extension
		String destinationFile;
		if (obj instanceof IFile) {
			destinationFile = ResourcesPlugin.getWorkspace().getRoot()
					.getLocation().toString()
					.concat(((IFile) obj).getFullPath().toOSString())
					.concat(".sh");
			filesystem = iFileToModel((IFile) obj);
		} else {
			System.out.println("Error no se reconoce:   "
					+ obj.getClass().getName());
			return;
		}
		// Generar la plantilla
		templateGenerate(filesystem, destinationFile);
		
		MessageDialog.openInformation(shell, "Example7", "Generate was executed.");
	}

	public static Filesystem iFileToModel(IFile iFile) {
		// La parte comentada solo es necesaria si el modelo no está registrado
		// Inicializar el modelo
		// FilesystemFactory.eINSTANCE.eClass();
		// // Registrar la factoría del recurso XMI para la extension
		// org.eclipse.emf.ecore.resource.Resource.Factory.Registry reg =
		// org.eclipse.emf.ecore.resource.Resource.Factory.Registry.INSTANCE;
		// Map<String, Object> m = reg.getExtensionToFactoryMap();
		// m.put("filesystem", new XMIResourceFactoryImpl());

		// Obtener un nuevo resource set
		ResourceSet resSet = new ResourceSetImpl();
		// Obtener el recurso
		org.eclipse.emf.ecore.resource.Resource resource = resSet.getResource(
				URI.createURI("platform:/resource"
						+ iFile.getFullPath().toPortableString()), true);
		// Coger el primer elemento del modelo y hacer un cast del tipo adecuado
		return (Filesystem) resource.getContents().get(0);
	}

	private void templateGenerate(Filesystem filesystem, String destinationFile) {
		Filesystem2Script template = new Filesystem2Script();
		FileWriter output;
		BufferedWriter writer;
		try {
			output = new FileWriter(destinationFile);
			writer = new BufferedWriter(output);
			writer.write(template.generate(filesystem));
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
		this.selection = (IStructuredSelection) selection;
	}

}
