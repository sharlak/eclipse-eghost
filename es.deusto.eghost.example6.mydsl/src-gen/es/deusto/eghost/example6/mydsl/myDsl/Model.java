/**
 */
package es.deusto.eghost.example6.mydsl.myDsl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.deusto.eghost.example6.mydsl.myDsl.Model#getExpr <em>Expr</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.deusto.eghost.example6.mydsl.myDsl.MyDslPackage#getModel()
 * @model
 * @generated
 */
public interface Model extends EObject
{
  /**
   * Returns the value of the '<em><b>Expr</b></em>' containment reference list.
   * The list contents are of type {@link es.deusto.eghost.example6.mydsl.myDsl.Expression}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Expr</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expr</em>' containment reference list.
   * @see es.deusto.eghost.example6.mydsl.myDsl.MyDslPackage#getModel_Expr()
   * @model containment="true"
   * @generated
   */
  EList<Expression> getExpr();

} // Model
