package es.deusto.eghost.example6.mydsl.serializer;

import com.google.inject.Inject;
import es.deusto.eghost.example6.mydsl.services.MyDslGrammarAccess;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.IGrammarAccess;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.AbstractElementAlias;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.AlternativeAlias;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.TokenAlias;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynNavigable;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynTransition;
import org.eclipse.xtext.serializer.sequencer.AbstractSyntacticSequencer;

@SuppressWarnings("all")
public class MyDslSyntacticSequencer extends AbstractSyntacticSequencer {

	protected MyDslGrammarAccess grammarAccess;
	protected AbstractElementAlias match_ArithmeticExpr_AsteriskKeyword_1_1_2_or_HyphenMinusKeyword_1_1_1_or_PlusSignKeyword_1_1_0_or_SolidusKeyword_1_1_3;
	protected AbstractElementAlias match_PrimaryExpr_LeftParenthesisKeyword_0_0_a;
	protected AbstractElementAlias match_PrimaryExpr_LeftParenthesisKeyword_0_0_p;
	
	@Inject
	protected void init(IGrammarAccess access) {
		grammarAccess = (MyDslGrammarAccess) access;
		match_ArithmeticExpr_AsteriskKeyword_1_1_2_or_HyphenMinusKeyword_1_1_1_or_PlusSignKeyword_1_1_0_or_SolidusKeyword_1_1_3 = new AlternativeAlias(false, false, new TokenAlias(false, false, grammarAccess.getArithmeticExprAccess().getAsteriskKeyword_1_1_2()), new TokenAlias(false, false, grammarAccess.getArithmeticExprAccess().getHyphenMinusKeyword_1_1_1()), new TokenAlias(false, false, grammarAccess.getArithmeticExprAccess().getPlusSignKeyword_1_1_0()), new TokenAlias(false, false, grammarAccess.getArithmeticExprAccess().getSolidusKeyword_1_1_3()));
		match_PrimaryExpr_LeftParenthesisKeyword_0_0_a = new TokenAlias(true, true, grammarAccess.getPrimaryExprAccess().getLeftParenthesisKeyword_0_0());
		match_PrimaryExpr_LeftParenthesisKeyword_0_0_p = new TokenAlias(true, false, grammarAccess.getPrimaryExprAccess().getLeftParenthesisKeyword_0_0());
	}
	
	@Override
	protected String getUnassignedRuleCallToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		return "";
	}
	
	
	@Override
	protected void emitUnassignedTokens(EObject semanticObject, ISynTransition transition, INode fromNode, INode toNode) {
		if (transition.getAmbiguousSyntaxes().isEmpty()) return;
		List<INode> transitionNodes = collectNodes(fromNode, toNode);
		for (AbstractElementAlias syntax : transition.getAmbiguousSyntaxes()) {
			List<INode> syntaxNodes = getNodesFor(transitionNodes, syntax);
			if(match_ArithmeticExpr_AsteriskKeyword_1_1_2_or_HyphenMinusKeyword_1_1_1_or_PlusSignKeyword_1_1_0_or_SolidusKeyword_1_1_3.equals(syntax))
				emit_ArithmeticExpr_AsteriskKeyword_1_1_2_or_HyphenMinusKeyword_1_1_1_or_PlusSignKeyword_1_1_0_or_SolidusKeyword_1_1_3(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_PrimaryExpr_LeftParenthesisKeyword_0_0_a.equals(syntax))
				emit_PrimaryExpr_LeftParenthesisKeyword_0_0_a(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_PrimaryExpr_LeftParenthesisKeyword_0_0_p.equals(syntax))
				emit_PrimaryExpr_LeftParenthesisKeyword_0_0_p(semanticObject, getLastNavigableState(), syntaxNodes);
			else acceptNodes(getLastNavigableState(), syntaxNodes);
		}
	}

	/**
	 * Syntax:
	 *     '*' | '+' | '/' | '-'
	 */
	protected void emit_ArithmeticExpr_AsteriskKeyword_1_1_2_or_HyphenMinusKeyword_1_1_1_or_PlusSignKeyword_1_1_0_or_SolidusKeyword_1_1_3(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     '('*
	 */
	protected void emit_PrimaryExpr_LeftParenthesisKeyword_0_0_a(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     '('+
	 */
	protected void emit_PrimaryExpr_LeftParenthesisKeyword_0_0_p(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
}
