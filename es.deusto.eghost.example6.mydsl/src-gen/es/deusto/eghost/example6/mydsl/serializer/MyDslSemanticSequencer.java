package es.deusto.eghost.example6.mydsl.serializer;

import com.google.inject.Inject;
import com.google.inject.Provider;
import es.deusto.eghost.example6.mydsl.myDsl.AbstactArithmeticExpr;
import es.deusto.eghost.example6.mydsl.myDsl.Expression;
import es.deusto.eghost.example6.mydsl.myDsl.Model;
import es.deusto.eghost.example6.mydsl.myDsl.MyDslPackage;
import es.deusto.eghost.example6.mydsl.myDsl.Op;
import es.deusto.eghost.example6.mydsl.services.MyDslGrammarAccess;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.serializer.acceptor.ISemanticSequenceAcceptor;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.diagnostic.ISemanticSequencerDiagnosticProvider;
import org.eclipse.xtext.serializer.diagnostic.ISerializationDiagnostic.Acceptor;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.GenericSequencer;
import org.eclipse.xtext.serializer.sequencer.ISemanticNodeProvider.INodesForEObjectProvider;
import org.eclipse.xtext.serializer.sequencer.ISemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;

@SuppressWarnings("all")
public class MyDslSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private MyDslGrammarAccess grammarAccess;
	
	public void createSequence(EObject context, EObject semanticObject) {
		if(semanticObject.eClass().getEPackage() == MyDslPackage.eINSTANCE) switch(semanticObject.eClass().getClassifierID()) {
			case MyDslPackage.ABSTACT_ARITHMETIC_EXPR:
				if(context == grammarAccess.getAbstactArithmeticExprRule() ||
				   context == grammarAccess.getArithmeticExprRule() ||
				   context == grammarAccess.getArithmeticExprAccess().getOpLeftAction_1_0() ||
				   context == grammarAccess.getPrimaryExprRule()) {
					sequence_PrimaryExpr(context, (AbstactArithmeticExpr) semanticObject); 
					return; 
				}
				else break;
			case MyDslPackage.EXPRESSION:
				if(context == grammarAccess.getExpressionRule()) {
					sequence_Expression(context, (Expression) semanticObject); 
					return; 
				}
				else break;
			case MyDslPackage.MODEL:
				if(context == grammarAccess.getModelRule()) {
					sequence_Model(context, (Model) semanticObject); 
					return; 
				}
				else break;
			case MyDslPackage.OP:
				if(context == grammarAccess.getAbstactArithmeticExprRule() ||
				   context == grammarAccess.getArithmeticExprRule() ||
				   context == grammarAccess.getArithmeticExprAccess().getOpLeftAction_1_0() ||
				   context == grammarAccess.getPrimaryExprRule()) {
					sequence_ArithmeticExpr(context, (Op) semanticObject); 
					return; 
				}
				else break;
			}
		if (errorAcceptor != null) errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Constraint:
	 *     (left=ArithmeticExpr_Op_1_0 right=PrimaryExpr)
	 */
	protected void sequence_ArithmeticExpr(EObject context, Op semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (var=ID expr+=AbstactArithmeticExpr*)
	 */
	protected void sequence_Expression(EObject context, Expression semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     expr+=Expression*
	 */
	protected void sequence_Model(EObject context, Model semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     value=INT
	 */
	protected void sequence_PrimaryExpr(EObject context, AbstactArithmeticExpr semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, MyDslPackage.Literals.ABSTACT_ARITHMETIC_EXPR__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MyDslPackage.Literals.ABSTACT_ARITHMETIC_EXPR__VALUE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getPrimaryExprAccess().getValueINTTerminalRuleCall_1_0(), semanticObject.getValue());
		feeder.finish();
	}
}
