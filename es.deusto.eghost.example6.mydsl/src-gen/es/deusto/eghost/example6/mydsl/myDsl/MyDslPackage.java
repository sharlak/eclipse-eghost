/**
 */
package es.deusto.eghost.example6.mydsl.myDsl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see es.deusto.eghost.example6.mydsl.myDsl.MyDslFactory
 * @model kind="package"
 * @generated
 */
public interface MyDslPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "myDsl";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.deusto.es/eghost/example6/mydsl/MyDsl";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "myDsl";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  MyDslPackage eINSTANCE = es.deusto.eghost.example6.mydsl.myDsl.impl.MyDslPackageImpl.init();

  /**
   * The meta object id for the '{@link es.deusto.eghost.example6.mydsl.myDsl.impl.ModelImpl <em>Model</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.deusto.eghost.example6.mydsl.myDsl.impl.ModelImpl
   * @see es.deusto.eghost.example6.mydsl.myDsl.impl.MyDslPackageImpl#getModel()
   * @generated
   */
  int MODEL = 0;

  /**
   * The feature id for the '<em><b>Expr</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL__EXPR = 0;

  /**
   * The number of structural features of the '<em>Model</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link es.deusto.eghost.example6.mydsl.myDsl.impl.ExpressionImpl <em>Expression</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.deusto.eghost.example6.mydsl.myDsl.impl.ExpressionImpl
   * @see es.deusto.eghost.example6.mydsl.myDsl.impl.MyDslPackageImpl#getExpression()
   * @generated
   */
  int EXPRESSION = 1;

  /**
   * The feature id for the '<em><b>Var</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION__VAR = 0;

  /**
   * The feature id for the '<em><b>Expr</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION__EXPR = 1;

  /**
   * The number of structural features of the '<em>Expression</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXPRESSION_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link es.deusto.eghost.example6.mydsl.myDsl.impl.AbstactArithmeticExprImpl <em>Abstact Arithmetic Expr</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.deusto.eghost.example6.mydsl.myDsl.impl.AbstactArithmeticExprImpl
   * @see es.deusto.eghost.example6.mydsl.myDsl.impl.MyDslPackageImpl#getAbstactArithmeticExpr()
   * @generated
   */
  int ABSTACT_ARITHMETIC_EXPR = 2;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ABSTACT_ARITHMETIC_EXPR__VALUE = 0;

  /**
   * The number of structural features of the '<em>Abstact Arithmetic Expr</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ABSTACT_ARITHMETIC_EXPR_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link es.deusto.eghost.example6.mydsl.myDsl.impl.OpImpl <em>Op</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.deusto.eghost.example6.mydsl.myDsl.impl.OpImpl
   * @see es.deusto.eghost.example6.mydsl.myDsl.impl.MyDslPackageImpl#getOp()
   * @generated
   */
  int OP = 3;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OP__VALUE = ABSTACT_ARITHMETIC_EXPR__VALUE;

  /**
   * The feature id for the '<em><b>Left</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OP__LEFT = ABSTACT_ARITHMETIC_EXPR_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Right</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OP__RIGHT = ABSTACT_ARITHMETIC_EXPR_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Op</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OP_FEATURE_COUNT = ABSTACT_ARITHMETIC_EXPR_FEATURE_COUNT + 2;


  /**
   * Returns the meta object for class '{@link es.deusto.eghost.example6.mydsl.myDsl.Model <em>Model</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Model</em>'.
   * @see es.deusto.eghost.example6.mydsl.myDsl.Model
   * @generated
   */
  EClass getModel();

  /**
   * Returns the meta object for the containment reference list '{@link es.deusto.eghost.example6.mydsl.myDsl.Model#getExpr <em>Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Expr</em>'.
   * @see es.deusto.eghost.example6.mydsl.myDsl.Model#getExpr()
   * @see #getModel()
   * @generated
   */
  EReference getModel_Expr();

  /**
   * Returns the meta object for class '{@link es.deusto.eghost.example6.mydsl.myDsl.Expression <em>Expression</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Expression</em>'.
   * @see es.deusto.eghost.example6.mydsl.myDsl.Expression
   * @generated
   */
  EClass getExpression();

  /**
   * Returns the meta object for the attribute '{@link es.deusto.eghost.example6.mydsl.myDsl.Expression#getVar <em>Var</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Var</em>'.
   * @see es.deusto.eghost.example6.mydsl.myDsl.Expression#getVar()
   * @see #getExpression()
   * @generated
   */
  EAttribute getExpression_Var();

  /**
   * Returns the meta object for the containment reference list '{@link es.deusto.eghost.example6.mydsl.myDsl.Expression#getExpr <em>Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Expr</em>'.
   * @see es.deusto.eghost.example6.mydsl.myDsl.Expression#getExpr()
   * @see #getExpression()
   * @generated
   */
  EReference getExpression_Expr();

  /**
   * Returns the meta object for class '{@link es.deusto.eghost.example6.mydsl.myDsl.AbstactArithmeticExpr <em>Abstact Arithmetic Expr</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Abstact Arithmetic Expr</em>'.
   * @see es.deusto.eghost.example6.mydsl.myDsl.AbstactArithmeticExpr
   * @generated
   */
  EClass getAbstactArithmeticExpr();

  /**
   * Returns the meta object for the attribute '{@link es.deusto.eghost.example6.mydsl.myDsl.AbstactArithmeticExpr#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see es.deusto.eghost.example6.mydsl.myDsl.AbstactArithmeticExpr#getValue()
   * @see #getAbstactArithmeticExpr()
   * @generated
   */
  EAttribute getAbstactArithmeticExpr_Value();

  /**
   * Returns the meta object for class '{@link es.deusto.eghost.example6.mydsl.myDsl.Op <em>Op</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Op</em>'.
   * @see es.deusto.eghost.example6.mydsl.myDsl.Op
   * @generated
   */
  EClass getOp();

  /**
   * Returns the meta object for the containment reference '{@link es.deusto.eghost.example6.mydsl.myDsl.Op#getLeft <em>Left</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Left</em>'.
   * @see es.deusto.eghost.example6.mydsl.myDsl.Op#getLeft()
   * @see #getOp()
   * @generated
   */
  EReference getOp_Left();

  /**
   * Returns the meta object for the containment reference '{@link es.deusto.eghost.example6.mydsl.myDsl.Op#getRight <em>Right</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Right</em>'.
   * @see es.deusto.eghost.example6.mydsl.myDsl.Op#getRight()
   * @see #getOp()
   * @generated
   */
  EReference getOp_Right();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  MyDslFactory getMyDslFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link es.deusto.eghost.example6.mydsl.myDsl.impl.ModelImpl <em>Model</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.deusto.eghost.example6.mydsl.myDsl.impl.ModelImpl
     * @see es.deusto.eghost.example6.mydsl.myDsl.impl.MyDslPackageImpl#getModel()
     * @generated
     */
    EClass MODEL = eINSTANCE.getModel();

    /**
     * The meta object literal for the '<em><b>Expr</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MODEL__EXPR = eINSTANCE.getModel_Expr();

    /**
     * The meta object literal for the '{@link es.deusto.eghost.example6.mydsl.myDsl.impl.ExpressionImpl <em>Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.deusto.eghost.example6.mydsl.myDsl.impl.ExpressionImpl
     * @see es.deusto.eghost.example6.mydsl.myDsl.impl.MyDslPackageImpl#getExpression()
     * @generated
     */
    EClass EXPRESSION = eINSTANCE.getExpression();

    /**
     * The meta object literal for the '<em><b>Var</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute EXPRESSION__VAR = eINSTANCE.getExpression_Var();

    /**
     * The meta object literal for the '<em><b>Expr</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXPRESSION__EXPR = eINSTANCE.getExpression_Expr();

    /**
     * The meta object literal for the '{@link es.deusto.eghost.example6.mydsl.myDsl.impl.AbstactArithmeticExprImpl <em>Abstact Arithmetic Expr</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.deusto.eghost.example6.mydsl.myDsl.impl.AbstactArithmeticExprImpl
     * @see es.deusto.eghost.example6.mydsl.myDsl.impl.MyDslPackageImpl#getAbstactArithmeticExpr()
     * @generated
     */
    EClass ABSTACT_ARITHMETIC_EXPR = eINSTANCE.getAbstactArithmeticExpr();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ABSTACT_ARITHMETIC_EXPR__VALUE = eINSTANCE.getAbstactArithmeticExpr_Value();

    /**
     * The meta object literal for the '{@link es.deusto.eghost.example6.mydsl.myDsl.impl.OpImpl <em>Op</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.deusto.eghost.example6.mydsl.myDsl.impl.OpImpl
     * @see es.deusto.eghost.example6.mydsl.myDsl.impl.MyDslPackageImpl#getOp()
     * @generated
     */
    EClass OP = eINSTANCE.getOp();

    /**
     * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OP__LEFT = eINSTANCE.getOp_Left();

    /**
     * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OP__RIGHT = eINSTANCE.getOp_Right();

  }

} //MyDslPackage
