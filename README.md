![Alt eGhost](http://oi62.tinypic.com/31456is.jpg)
#README#
This repository contains the examples of the course: [Eclipse Avanzado](https://sites.google.com/a/deusto.es/e-ghost-2014/cursos/eclipse-avanzado) (in Spanish) presented in e-Ghost 2014 Summer Lessons

You can find [the slides of the the presentation of the course](https://bitbucket.org/sharlak/eclipse-eghost/downloads/2014-Curso-Eclipse-Avanzado-e-Ghost.odp) (also in Spanish) in the downloads section of this repository.

## Course contents ##
* Introduction
    * Introduction to Eclipse
    * Plugin development
    * Interaction between plugins
* Modeling
    * Introduction to Modeling
    * Introduction to Modeling in Eclipse
    * EMF/GMF/Eugenia/emfatic
    * Xtext
* Code generation
    * Introduction to transformations
    * JET

## Dependencies ##
The environment used to make this examples was:

* Eclipse Kepler Modeling Tools (build id: 20140224-0627)
* Plugins  (used versions)
    * Eclipse Modeling Tools (2.0.2.20140224-0000)
    * Emfatic (Incubation)	(0.8.0.201302100848)
    * Epsilon Wizard Language GMF Integration (1.1.0.201309101707)
    * Eugenia (1.1.0.201309101707)
    * Graphical Modeling Framework (GMF) Tooling SDK (3.1.0.201402192033)
    * Java Emitter Templates (JET) (1.1.1.v201101271243-5319sC7HCAGERASDK3924)
    * Java Emitter Templates (JET) SDK (1.1.1.v201101271243-42186A5NsKEQPHRQeFaJS4A35)
    * Jave Emitter Templates (JET) Developer Resources (1.1.1.v201101271243)
    * Xtext SDK (2.4.3.v201309030823)
* Java 7 (openjdk-7-jdk)
You will need compatible versions of these plugins in order to make the examples run.
##Set up & Run ##
###Setting up: ###
* Clone the repository
* Open Eclipse
* Import -> Existing Projects into Workspace and select the root folder of the repository
###Running: ###
* Run as -> Eclipse Application (select the examples you want to run)
* Important: Example5 is a evolution of Example4 and both define the same model, so it is recommended to avoid running them at the same time
* If you understand Spanish you can check the slides for detailed explanations of each example
## Solving problems ##
* Example4 (EMF) and Example5 (GMF) define the same models, so close one of them when running the examples to avoid model registration errors. 
* If you do not have the same (or compatible) versions of these plugins you may have some compilation problems with the examples. If this is your case, try:
    * save the models of the projects that are causing problems in an external editor
    * delete those projects
    * recreate them
    * copy back the models 
    * regenerate
## Contributions ##
If you have any correction or contribution to these examples, please feel free to post a pull request.