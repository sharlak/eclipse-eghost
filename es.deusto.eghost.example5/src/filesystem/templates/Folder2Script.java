package filesystem.templates;

import filesystem.*;;

public class Folder2Script
{
  protected static String nl;
  public static synchronized Folder2Script create(String lineSeparator)
  {
    nl = lineSeparator;
    Folder2Script result = new Folder2Script();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "mkdir ";
  protected final String TEXT_2 = NL + "cd    ";
  protected final String TEXT_3 = NL + "\t";
  protected final String TEXT_4 = NL;
  protected final String TEXT_5 = NL + "\t";
  protected final String TEXT_6 = NL + "touch ";
  protected final String TEXT_7 = NL + "\t";
  protected final String TEXT_8 = NL + "cd ..";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     Folder folder = (Folder) argument; 
    stringBuffer.append(TEXT_1);
    stringBuffer.append( folder.getName() );
    stringBuffer.append(TEXT_2);
    stringBuffer.append( folder.getName() );
     for(File file: folder.getContents()){ 
    stringBuffer.append(TEXT_3);
     if(file instanceof Drive ||
                file instanceof Folder){ 
    stringBuffer.append(TEXT_4);
    stringBuffer.append( new Folder2Script().generate(file) );
    stringBuffer.append(TEXT_5);
     } else if(file instanceof File &&
			!(file instanceof Sync)){ 
    stringBuffer.append(TEXT_6);
    stringBuffer.append( file.getName() );
    stringBuffer.append(TEXT_7);
     } 
     } 
    stringBuffer.append(TEXT_8);
    return stringBuffer.toString();
  }
}
