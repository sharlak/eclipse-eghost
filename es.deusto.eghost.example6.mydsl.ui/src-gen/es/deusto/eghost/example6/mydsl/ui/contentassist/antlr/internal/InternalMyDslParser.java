package es.deusto.eghost.example6.mydsl.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import es.deusto.eghost.example6.mydsl.services.MyDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMyDslParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'+'", "'-'", "'*'", "'/'", "'='", "';'", "'('", "')'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_STRING=6;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=5;
    public static final int RULE_WS=9;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;

    // delegates
    // delegators


        public InternalMyDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMyDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMyDslParser.tokenNames; }
    public String getGrammarFileName() { return "../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g"; }


     
     	private MyDslGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(MyDslGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleModel"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:60:1: entryRuleModel : ruleModel EOF ;
    public final void entryRuleModel() throws RecognitionException {
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:61:1: ( ruleModel EOF )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:62:1: ruleModel EOF
            {
             before(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_ruleModel_in_entryRuleModel61);
            ruleModel();

            state._fsp--;

             after(grammarAccess.getModelRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleModel68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:69:1: ruleModel : ( ( rule__Model__ExprAssignment )* ) ;
    public final void ruleModel() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:73:2: ( ( ( rule__Model__ExprAssignment )* ) )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:74:1: ( ( rule__Model__ExprAssignment )* )
            {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:74:1: ( ( rule__Model__ExprAssignment )* )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:75:1: ( rule__Model__ExprAssignment )*
            {
             before(grammarAccess.getModelAccess().getExprAssignment()); 
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:76:1: ( rule__Model__ExprAssignment )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==RULE_ID) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:76:2: rule__Model__ExprAssignment
            	    {
            	    pushFollow(FOLLOW_rule__Model__ExprAssignment_in_ruleModel94);
            	    rule__Model__ExprAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getModelAccess().getExprAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleExpression"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:88:1: entryRuleExpression : ruleExpression EOF ;
    public final void entryRuleExpression() throws RecognitionException {
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:89:1: ( ruleExpression EOF )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:90:1: ruleExpression EOF
            {
             before(grammarAccess.getExpressionRule()); 
            pushFollow(FOLLOW_ruleExpression_in_entryRuleExpression122);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getExpressionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleExpression129); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:97:1: ruleExpression : ( ( rule__Expression__Group__0 ) ) ;
    public final void ruleExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:101:2: ( ( ( rule__Expression__Group__0 ) ) )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:102:1: ( ( rule__Expression__Group__0 ) )
            {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:102:1: ( ( rule__Expression__Group__0 ) )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:103:1: ( rule__Expression__Group__0 )
            {
             before(grammarAccess.getExpressionAccess().getGroup()); 
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:104:1: ( rule__Expression__Group__0 )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:104:2: rule__Expression__Group__0
            {
            pushFollow(FOLLOW_rule__Expression__Group__0_in_ruleExpression155);
            rule__Expression__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getExpressionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleAbstactArithmeticExpr"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:116:1: entryRuleAbstactArithmeticExpr : ruleAbstactArithmeticExpr EOF ;
    public final void entryRuleAbstactArithmeticExpr() throws RecognitionException {
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:117:1: ( ruleAbstactArithmeticExpr EOF )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:118:1: ruleAbstactArithmeticExpr EOF
            {
             before(grammarAccess.getAbstactArithmeticExprRule()); 
            pushFollow(FOLLOW_ruleAbstactArithmeticExpr_in_entryRuleAbstactArithmeticExpr182);
            ruleAbstactArithmeticExpr();

            state._fsp--;

             after(grammarAccess.getAbstactArithmeticExprRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleAbstactArithmeticExpr189); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAbstactArithmeticExpr"


    // $ANTLR start "ruleAbstactArithmeticExpr"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:125:1: ruleAbstactArithmeticExpr : ( ruleArithmeticExpr ) ;
    public final void ruleAbstactArithmeticExpr() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:129:2: ( ( ruleArithmeticExpr ) )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:130:1: ( ruleArithmeticExpr )
            {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:130:1: ( ruleArithmeticExpr )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:131:1: ruleArithmeticExpr
            {
             before(grammarAccess.getAbstactArithmeticExprAccess().getArithmeticExprParserRuleCall()); 
            pushFollow(FOLLOW_ruleArithmeticExpr_in_ruleAbstactArithmeticExpr215);
            ruleArithmeticExpr();

            state._fsp--;

             after(grammarAccess.getAbstactArithmeticExprAccess().getArithmeticExprParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAbstactArithmeticExpr"


    // $ANTLR start "entryRuleArithmeticExpr"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:144:1: entryRuleArithmeticExpr : ruleArithmeticExpr EOF ;
    public final void entryRuleArithmeticExpr() throws RecognitionException {
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:145:1: ( ruleArithmeticExpr EOF )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:146:1: ruleArithmeticExpr EOF
            {
             before(grammarAccess.getArithmeticExprRule()); 
            pushFollow(FOLLOW_ruleArithmeticExpr_in_entryRuleArithmeticExpr241);
            ruleArithmeticExpr();

            state._fsp--;

             after(grammarAccess.getArithmeticExprRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleArithmeticExpr248); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleArithmeticExpr"


    // $ANTLR start "ruleArithmeticExpr"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:153:1: ruleArithmeticExpr : ( ( rule__ArithmeticExpr__Group__0 ) ) ;
    public final void ruleArithmeticExpr() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:157:2: ( ( ( rule__ArithmeticExpr__Group__0 ) ) )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:158:1: ( ( rule__ArithmeticExpr__Group__0 ) )
            {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:158:1: ( ( rule__ArithmeticExpr__Group__0 ) )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:159:1: ( rule__ArithmeticExpr__Group__0 )
            {
             before(grammarAccess.getArithmeticExprAccess().getGroup()); 
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:160:1: ( rule__ArithmeticExpr__Group__0 )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:160:2: rule__ArithmeticExpr__Group__0
            {
            pushFollow(FOLLOW_rule__ArithmeticExpr__Group__0_in_ruleArithmeticExpr274);
            rule__ArithmeticExpr__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getArithmeticExprAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleArithmeticExpr"


    // $ANTLR start "entryRulePrimaryExpr"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:172:1: entryRulePrimaryExpr : rulePrimaryExpr EOF ;
    public final void entryRulePrimaryExpr() throws RecognitionException {
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:173:1: ( rulePrimaryExpr EOF )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:174:1: rulePrimaryExpr EOF
            {
             before(grammarAccess.getPrimaryExprRule()); 
            pushFollow(FOLLOW_rulePrimaryExpr_in_entryRulePrimaryExpr301);
            rulePrimaryExpr();

            state._fsp--;

             after(grammarAccess.getPrimaryExprRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulePrimaryExpr308); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePrimaryExpr"


    // $ANTLR start "rulePrimaryExpr"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:181:1: rulePrimaryExpr : ( ( rule__PrimaryExpr__Alternatives ) ) ;
    public final void rulePrimaryExpr() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:185:2: ( ( ( rule__PrimaryExpr__Alternatives ) ) )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:186:1: ( ( rule__PrimaryExpr__Alternatives ) )
            {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:186:1: ( ( rule__PrimaryExpr__Alternatives ) )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:187:1: ( rule__PrimaryExpr__Alternatives )
            {
             before(grammarAccess.getPrimaryExprAccess().getAlternatives()); 
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:188:1: ( rule__PrimaryExpr__Alternatives )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:188:2: rule__PrimaryExpr__Alternatives
            {
            pushFollow(FOLLOW_rule__PrimaryExpr__Alternatives_in_rulePrimaryExpr334);
            rule__PrimaryExpr__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPrimaryExprAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePrimaryExpr"


    // $ANTLR start "rule__ArithmeticExpr__Alternatives_1_1"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:200:1: rule__ArithmeticExpr__Alternatives_1_1 : ( ( '+' ) | ( '-' ) | ( '*' ) | ( '/' ) );
    public final void rule__ArithmeticExpr__Alternatives_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:204:1: ( ( '+' ) | ( '-' ) | ( '*' ) | ( '/' ) )
            int alt2=4;
            switch ( input.LA(1) ) {
            case 11:
                {
                alt2=1;
                }
                break;
            case 12:
                {
                alt2=2;
                }
                break;
            case 13:
                {
                alt2=3;
                }
                break;
            case 14:
                {
                alt2=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:205:1: ( '+' )
                    {
                    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:205:1: ( '+' )
                    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:206:1: '+'
                    {
                     before(grammarAccess.getArithmeticExprAccess().getPlusSignKeyword_1_1_0()); 
                    match(input,11,FOLLOW_11_in_rule__ArithmeticExpr__Alternatives_1_1371); 
                     after(grammarAccess.getArithmeticExprAccess().getPlusSignKeyword_1_1_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:213:6: ( '-' )
                    {
                    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:213:6: ( '-' )
                    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:214:1: '-'
                    {
                     before(grammarAccess.getArithmeticExprAccess().getHyphenMinusKeyword_1_1_1()); 
                    match(input,12,FOLLOW_12_in_rule__ArithmeticExpr__Alternatives_1_1391); 
                     after(grammarAccess.getArithmeticExprAccess().getHyphenMinusKeyword_1_1_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:221:6: ( '*' )
                    {
                    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:221:6: ( '*' )
                    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:222:1: '*'
                    {
                     before(grammarAccess.getArithmeticExprAccess().getAsteriskKeyword_1_1_2()); 
                    match(input,13,FOLLOW_13_in_rule__ArithmeticExpr__Alternatives_1_1411); 
                     after(grammarAccess.getArithmeticExprAccess().getAsteriskKeyword_1_1_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:229:6: ( '/' )
                    {
                    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:229:6: ( '/' )
                    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:230:1: '/'
                    {
                     before(grammarAccess.getArithmeticExprAccess().getSolidusKeyword_1_1_3()); 
                    match(input,14,FOLLOW_14_in_rule__ArithmeticExpr__Alternatives_1_1431); 
                     after(grammarAccess.getArithmeticExprAccess().getSolidusKeyword_1_1_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArithmeticExpr__Alternatives_1_1"


    // $ANTLR start "rule__PrimaryExpr__Alternatives"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:242:1: rule__PrimaryExpr__Alternatives : ( ( ( rule__PrimaryExpr__Group_0__0 ) ) | ( ( rule__PrimaryExpr__ValueAssignment_1 ) ) );
    public final void rule__PrimaryExpr__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:246:1: ( ( ( rule__PrimaryExpr__Group_0__0 ) ) | ( ( rule__PrimaryExpr__ValueAssignment_1 ) ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==17) ) {
                alt3=1;
            }
            else if ( (LA3_0==RULE_INT) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:247:1: ( ( rule__PrimaryExpr__Group_0__0 ) )
                    {
                    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:247:1: ( ( rule__PrimaryExpr__Group_0__0 ) )
                    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:248:1: ( rule__PrimaryExpr__Group_0__0 )
                    {
                     before(grammarAccess.getPrimaryExprAccess().getGroup_0()); 
                    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:249:1: ( rule__PrimaryExpr__Group_0__0 )
                    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:249:2: rule__PrimaryExpr__Group_0__0
                    {
                    pushFollow(FOLLOW_rule__PrimaryExpr__Group_0__0_in_rule__PrimaryExpr__Alternatives465);
                    rule__PrimaryExpr__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryExprAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:253:6: ( ( rule__PrimaryExpr__ValueAssignment_1 ) )
                    {
                    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:253:6: ( ( rule__PrimaryExpr__ValueAssignment_1 ) )
                    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:254:1: ( rule__PrimaryExpr__ValueAssignment_1 )
                    {
                     before(grammarAccess.getPrimaryExprAccess().getValueAssignment_1()); 
                    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:255:1: ( rule__PrimaryExpr__ValueAssignment_1 )
                    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:255:2: rule__PrimaryExpr__ValueAssignment_1
                    {
                    pushFollow(FOLLOW_rule__PrimaryExpr__ValueAssignment_1_in_rule__PrimaryExpr__Alternatives483);
                    rule__PrimaryExpr__ValueAssignment_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getPrimaryExprAccess().getValueAssignment_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrimaryExpr__Alternatives"


    // $ANTLR start "rule__Expression__Group__0"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:266:1: rule__Expression__Group__0 : rule__Expression__Group__0__Impl rule__Expression__Group__1 ;
    public final void rule__Expression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:270:1: ( rule__Expression__Group__0__Impl rule__Expression__Group__1 )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:271:2: rule__Expression__Group__0__Impl rule__Expression__Group__1
            {
            pushFollow(FOLLOW_rule__Expression__Group__0__Impl_in_rule__Expression__Group__0514);
            rule__Expression__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Expression__Group__1_in_rule__Expression__Group__0517);
            rule__Expression__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group__0"


    // $ANTLR start "rule__Expression__Group__0__Impl"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:278:1: rule__Expression__Group__0__Impl : ( ( rule__Expression__VarAssignment_0 ) ) ;
    public final void rule__Expression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:282:1: ( ( ( rule__Expression__VarAssignment_0 ) ) )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:283:1: ( ( rule__Expression__VarAssignment_0 ) )
            {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:283:1: ( ( rule__Expression__VarAssignment_0 ) )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:284:1: ( rule__Expression__VarAssignment_0 )
            {
             before(grammarAccess.getExpressionAccess().getVarAssignment_0()); 
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:285:1: ( rule__Expression__VarAssignment_0 )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:285:2: rule__Expression__VarAssignment_0
            {
            pushFollow(FOLLOW_rule__Expression__VarAssignment_0_in_rule__Expression__Group__0__Impl544);
            rule__Expression__VarAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getExpressionAccess().getVarAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group__0__Impl"


    // $ANTLR start "rule__Expression__Group__1"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:295:1: rule__Expression__Group__1 : rule__Expression__Group__1__Impl rule__Expression__Group__2 ;
    public final void rule__Expression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:299:1: ( rule__Expression__Group__1__Impl rule__Expression__Group__2 )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:300:2: rule__Expression__Group__1__Impl rule__Expression__Group__2
            {
            pushFollow(FOLLOW_rule__Expression__Group__1__Impl_in_rule__Expression__Group__1574);
            rule__Expression__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Expression__Group__2_in_rule__Expression__Group__1577);
            rule__Expression__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group__1"


    // $ANTLR start "rule__Expression__Group__1__Impl"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:307:1: rule__Expression__Group__1__Impl : ( '=' ) ;
    public final void rule__Expression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:311:1: ( ( '=' ) )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:312:1: ( '=' )
            {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:312:1: ( '=' )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:313:1: '='
            {
             before(grammarAccess.getExpressionAccess().getEqualsSignKeyword_1()); 
            match(input,15,FOLLOW_15_in_rule__Expression__Group__1__Impl605); 
             after(grammarAccess.getExpressionAccess().getEqualsSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group__1__Impl"


    // $ANTLR start "rule__Expression__Group__2"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:326:1: rule__Expression__Group__2 : rule__Expression__Group__2__Impl rule__Expression__Group__3 ;
    public final void rule__Expression__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:330:1: ( rule__Expression__Group__2__Impl rule__Expression__Group__3 )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:331:2: rule__Expression__Group__2__Impl rule__Expression__Group__3
            {
            pushFollow(FOLLOW_rule__Expression__Group__2__Impl_in_rule__Expression__Group__2636);
            rule__Expression__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Expression__Group__3_in_rule__Expression__Group__2639);
            rule__Expression__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group__2"


    // $ANTLR start "rule__Expression__Group__2__Impl"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:338:1: rule__Expression__Group__2__Impl : ( ( rule__Expression__ExprAssignment_2 )* ) ;
    public final void rule__Expression__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:342:1: ( ( ( rule__Expression__ExprAssignment_2 )* ) )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:343:1: ( ( rule__Expression__ExprAssignment_2 )* )
            {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:343:1: ( ( rule__Expression__ExprAssignment_2 )* )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:344:1: ( rule__Expression__ExprAssignment_2 )*
            {
             before(grammarAccess.getExpressionAccess().getExprAssignment_2()); 
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:345:1: ( rule__Expression__ExprAssignment_2 )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==RULE_INT||LA4_0==17) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:345:2: rule__Expression__ExprAssignment_2
            	    {
            	    pushFollow(FOLLOW_rule__Expression__ExprAssignment_2_in_rule__Expression__Group__2__Impl666);
            	    rule__Expression__ExprAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getExpressionAccess().getExprAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group__2__Impl"


    // $ANTLR start "rule__Expression__Group__3"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:355:1: rule__Expression__Group__3 : rule__Expression__Group__3__Impl ;
    public final void rule__Expression__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:359:1: ( rule__Expression__Group__3__Impl )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:360:2: rule__Expression__Group__3__Impl
            {
            pushFollow(FOLLOW_rule__Expression__Group__3__Impl_in_rule__Expression__Group__3697);
            rule__Expression__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group__3"


    // $ANTLR start "rule__Expression__Group__3__Impl"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:366:1: rule__Expression__Group__3__Impl : ( ';' ) ;
    public final void rule__Expression__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:370:1: ( ( ';' ) )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:371:1: ( ';' )
            {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:371:1: ( ';' )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:372:1: ';'
            {
             before(grammarAccess.getExpressionAccess().getSemicolonKeyword_3()); 
            match(input,16,FOLLOW_16_in_rule__Expression__Group__3__Impl725); 
             after(grammarAccess.getExpressionAccess().getSemicolonKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__Group__3__Impl"


    // $ANTLR start "rule__ArithmeticExpr__Group__0"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:393:1: rule__ArithmeticExpr__Group__0 : rule__ArithmeticExpr__Group__0__Impl rule__ArithmeticExpr__Group__1 ;
    public final void rule__ArithmeticExpr__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:397:1: ( rule__ArithmeticExpr__Group__0__Impl rule__ArithmeticExpr__Group__1 )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:398:2: rule__ArithmeticExpr__Group__0__Impl rule__ArithmeticExpr__Group__1
            {
            pushFollow(FOLLOW_rule__ArithmeticExpr__Group__0__Impl_in_rule__ArithmeticExpr__Group__0764);
            rule__ArithmeticExpr__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ArithmeticExpr__Group__1_in_rule__ArithmeticExpr__Group__0767);
            rule__ArithmeticExpr__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArithmeticExpr__Group__0"


    // $ANTLR start "rule__ArithmeticExpr__Group__0__Impl"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:405:1: rule__ArithmeticExpr__Group__0__Impl : ( rulePrimaryExpr ) ;
    public final void rule__ArithmeticExpr__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:409:1: ( ( rulePrimaryExpr ) )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:410:1: ( rulePrimaryExpr )
            {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:410:1: ( rulePrimaryExpr )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:411:1: rulePrimaryExpr
            {
             before(grammarAccess.getArithmeticExprAccess().getPrimaryExprParserRuleCall_0()); 
            pushFollow(FOLLOW_rulePrimaryExpr_in_rule__ArithmeticExpr__Group__0__Impl794);
            rulePrimaryExpr();

            state._fsp--;

             after(grammarAccess.getArithmeticExprAccess().getPrimaryExprParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArithmeticExpr__Group__0__Impl"


    // $ANTLR start "rule__ArithmeticExpr__Group__1"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:422:1: rule__ArithmeticExpr__Group__1 : rule__ArithmeticExpr__Group__1__Impl ;
    public final void rule__ArithmeticExpr__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:426:1: ( rule__ArithmeticExpr__Group__1__Impl )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:427:2: rule__ArithmeticExpr__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__ArithmeticExpr__Group__1__Impl_in_rule__ArithmeticExpr__Group__1823);
            rule__ArithmeticExpr__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArithmeticExpr__Group__1"


    // $ANTLR start "rule__ArithmeticExpr__Group__1__Impl"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:433:1: rule__ArithmeticExpr__Group__1__Impl : ( ( rule__ArithmeticExpr__Group_1__0 )* ) ;
    public final void rule__ArithmeticExpr__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:437:1: ( ( ( rule__ArithmeticExpr__Group_1__0 )* ) )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:438:1: ( ( rule__ArithmeticExpr__Group_1__0 )* )
            {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:438:1: ( ( rule__ArithmeticExpr__Group_1__0 )* )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:439:1: ( rule__ArithmeticExpr__Group_1__0 )*
            {
             before(grammarAccess.getArithmeticExprAccess().getGroup_1()); 
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:440:1: ( rule__ArithmeticExpr__Group_1__0 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0>=11 && LA5_0<=14)) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:440:2: rule__ArithmeticExpr__Group_1__0
            	    {
            	    pushFollow(FOLLOW_rule__ArithmeticExpr__Group_1__0_in_rule__ArithmeticExpr__Group__1__Impl850);
            	    rule__ArithmeticExpr__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getArithmeticExprAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArithmeticExpr__Group__1__Impl"


    // $ANTLR start "rule__ArithmeticExpr__Group_1__0"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:454:1: rule__ArithmeticExpr__Group_1__0 : rule__ArithmeticExpr__Group_1__0__Impl rule__ArithmeticExpr__Group_1__1 ;
    public final void rule__ArithmeticExpr__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:458:1: ( rule__ArithmeticExpr__Group_1__0__Impl rule__ArithmeticExpr__Group_1__1 )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:459:2: rule__ArithmeticExpr__Group_1__0__Impl rule__ArithmeticExpr__Group_1__1
            {
            pushFollow(FOLLOW_rule__ArithmeticExpr__Group_1__0__Impl_in_rule__ArithmeticExpr__Group_1__0885);
            rule__ArithmeticExpr__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ArithmeticExpr__Group_1__1_in_rule__ArithmeticExpr__Group_1__0888);
            rule__ArithmeticExpr__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArithmeticExpr__Group_1__0"


    // $ANTLR start "rule__ArithmeticExpr__Group_1__0__Impl"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:466:1: rule__ArithmeticExpr__Group_1__0__Impl : ( () ) ;
    public final void rule__ArithmeticExpr__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:470:1: ( ( () ) )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:471:1: ( () )
            {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:471:1: ( () )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:472:1: ()
            {
             before(grammarAccess.getArithmeticExprAccess().getOpLeftAction_1_0()); 
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:473:1: ()
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:475:1: 
            {
            }

             after(grammarAccess.getArithmeticExprAccess().getOpLeftAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArithmeticExpr__Group_1__0__Impl"


    // $ANTLR start "rule__ArithmeticExpr__Group_1__1"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:485:1: rule__ArithmeticExpr__Group_1__1 : rule__ArithmeticExpr__Group_1__1__Impl rule__ArithmeticExpr__Group_1__2 ;
    public final void rule__ArithmeticExpr__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:489:1: ( rule__ArithmeticExpr__Group_1__1__Impl rule__ArithmeticExpr__Group_1__2 )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:490:2: rule__ArithmeticExpr__Group_1__1__Impl rule__ArithmeticExpr__Group_1__2
            {
            pushFollow(FOLLOW_rule__ArithmeticExpr__Group_1__1__Impl_in_rule__ArithmeticExpr__Group_1__1946);
            rule__ArithmeticExpr__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ArithmeticExpr__Group_1__2_in_rule__ArithmeticExpr__Group_1__1949);
            rule__ArithmeticExpr__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArithmeticExpr__Group_1__1"


    // $ANTLR start "rule__ArithmeticExpr__Group_1__1__Impl"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:497:1: rule__ArithmeticExpr__Group_1__1__Impl : ( ( rule__ArithmeticExpr__Alternatives_1_1 ) ) ;
    public final void rule__ArithmeticExpr__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:501:1: ( ( ( rule__ArithmeticExpr__Alternatives_1_1 ) ) )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:502:1: ( ( rule__ArithmeticExpr__Alternatives_1_1 ) )
            {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:502:1: ( ( rule__ArithmeticExpr__Alternatives_1_1 ) )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:503:1: ( rule__ArithmeticExpr__Alternatives_1_1 )
            {
             before(grammarAccess.getArithmeticExprAccess().getAlternatives_1_1()); 
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:504:1: ( rule__ArithmeticExpr__Alternatives_1_1 )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:504:2: rule__ArithmeticExpr__Alternatives_1_1
            {
            pushFollow(FOLLOW_rule__ArithmeticExpr__Alternatives_1_1_in_rule__ArithmeticExpr__Group_1__1__Impl976);
            rule__ArithmeticExpr__Alternatives_1_1();

            state._fsp--;


            }

             after(grammarAccess.getArithmeticExprAccess().getAlternatives_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArithmeticExpr__Group_1__1__Impl"


    // $ANTLR start "rule__ArithmeticExpr__Group_1__2"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:514:1: rule__ArithmeticExpr__Group_1__2 : rule__ArithmeticExpr__Group_1__2__Impl ;
    public final void rule__ArithmeticExpr__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:518:1: ( rule__ArithmeticExpr__Group_1__2__Impl )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:519:2: rule__ArithmeticExpr__Group_1__2__Impl
            {
            pushFollow(FOLLOW_rule__ArithmeticExpr__Group_1__2__Impl_in_rule__ArithmeticExpr__Group_1__21006);
            rule__ArithmeticExpr__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArithmeticExpr__Group_1__2"


    // $ANTLR start "rule__ArithmeticExpr__Group_1__2__Impl"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:525:1: rule__ArithmeticExpr__Group_1__2__Impl : ( ( rule__ArithmeticExpr__RightAssignment_1_2 ) ) ;
    public final void rule__ArithmeticExpr__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:529:1: ( ( ( rule__ArithmeticExpr__RightAssignment_1_2 ) ) )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:530:1: ( ( rule__ArithmeticExpr__RightAssignment_1_2 ) )
            {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:530:1: ( ( rule__ArithmeticExpr__RightAssignment_1_2 ) )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:531:1: ( rule__ArithmeticExpr__RightAssignment_1_2 )
            {
             before(grammarAccess.getArithmeticExprAccess().getRightAssignment_1_2()); 
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:532:1: ( rule__ArithmeticExpr__RightAssignment_1_2 )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:532:2: rule__ArithmeticExpr__RightAssignment_1_2
            {
            pushFollow(FOLLOW_rule__ArithmeticExpr__RightAssignment_1_2_in_rule__ArithmeticExpr__Group_1__2__Impl1033);
            rule__ArithmeticExpr__RightAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getArithmeticExprAccess().getRightAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArithmeticExpr__Group_1__2__Impl"


    // $ANTLR start "rule__PrimaryExpr__Group_0__0"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:548:1: rule__PrimaryExpr__Group_0__0 : rule__PrimaryExpr__Group_0__0__Impl rule__PrimaryExpr__Group_0__1 ;
    public final void rule__PrimaryExpr__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:552:1: ( rule__PrimaryExpr__Group_0__0__Impl rule__PrimaryExpr__Group_0__1 )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:553:2: rule__PrimaryExpr__Group_0__0__Impl rule__PrimaryExpr__Group_0__1
            {
            pushFollow(FOLLOW_rule__PrimaryExpr__Group_0__0__Impl_in_rule__PrimaryExpr__Group_0__01069);
            rule__PrimaryExpr__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__PrimaryExpr__Group_0__1_in_rule__PrimaryExpr__Group_0__01072);
            rule__PrimaryExpr__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrimaryExpr__Group_0__0"


    // $ANTLR start "rule__PrimaryExpr__Group_0__0__Impl"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:560:1: rule__PrimaryExpr__Group_0__0__Impl : ( '(' ) ;
    public final void rule__PrimaryExpr__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:564:1: ( ( '(' ) )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:565:1: ( '(' )
            {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:565:1: ( '(' )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:566:1: '('
            {
             before(grammarAccess.getPrimaryExprAccess().getLeftParenthesisKeyword_0_0()); 
            match(input,17,FOLLOW_17_in_rule__PrimaryExpr__Group_0__0__Impl1100); 
             after(grammarAccess.getPrimaryExprAccess().getLeftParenthesisKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrimaryExpr__Group_0__0__Impl"


    // $ANTLR start "rule__PrimaryExpr__Group_0__1"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:579:1: rule__PrimaryExpr__Group_0__1 : rule__PrimaryExpr__Group_0__1__Impl rule__PrimaryExpr__Group_0__2 ;
    public final void rule__PrimaryExpr__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:583:1: ( rule__PrimaryExpr__Group_0__1__Impl rule__PrimaryExpr__Group_0__2 )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:584:2: rule__PrimaryExpr__Group_0__1__Impl rule__PrimaryExpr__Group_0__2
            {
            pushFollow(FOLLOW_rule__PrimaryExpr__Group_0__1__Impl_in_rule__PrimaryExpr__Group_0__11131);
            rule__PrimaryExpr__Group_0__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__PrimaryExpr__Group_0__2_in_rule__PrimaryExpr__Group_0__11134);
            rule__PrimaryExpr__Group_0__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrimaryExpr__Group_0__1"


    // $ANTLR start "rule__PrimaryExpr__Group_0__1__Impl"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:591:1: rule__PrimaryExpr__Group_0__1__Impl : ( ruleArithmeticExpr ) ;
    public final void rule__PrimaryExpr__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:595:1: ( ( ruleArithmeticExpr ) )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:596:1: ( ruleArithmeticExpr )
            {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:596:1: ( ruleArithmeticExpr )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:597:1: ruleArithmeticExpr
            {
             before(grammarAccess.getPrimaryExprAccess().getArithmeticExprParserRuleCall_0_1()); 
            pushFollow(FOLLOW_ruleArithmeticExpr_in_rule__PrimaryExpr__Group_0__1__Impl1161);
            ruleArithmeticExpr();

            state._fsp--;

             after(grammarAccess.getPrimaryExprAccess().getArithmeticExprParserRuleCall_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrimaryExpr__Group_0__1__Impl"


    // $ANTLR start "rule__PrimaryExpr__Group_0__2"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:608:1: rule__PrimaryExpr__Group_0__2 : rule__PrimaryExpr__Group_0__2__Impl ;
    public final void rule__PrimaryExpr__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:612:1: ( rule__PrimaryExpr__Group_0__2__Impl )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:613:2: rule__PrimaryExpr__Group_0__2__Impl
            {
            pushFollow(FOLLOW_rule__PrimaryExpr__Group_0__2__Impl_in_rule__PrimaryExpr__Group_0__21190);
            rule__PrimaryExpr__Group_0__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrimaryExpr__Group_0__2"


    // $ANTLR start "rule__PrimaryExpr__Group_0__2__Impl"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:619:1: rule__PrimaryExpr__Group_0__2__Impl : ( ')' ) ;
    public final void rule__PrimaryExpr__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:623:1: ( ( ')' ) )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:624:1: ( ')' )
            {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:624:1: ( ')' )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:625:1: ')'
            {
             before(grammarAccess.getPrimaryExprAccess().getRightParenthesisKeyword_0_2()); 
            match(input,18,FOLLOW_18_in_rule__PrimaryExpr__Group_0__2__Impl1218); 
             after(grammarAccess.getPrimaryExprAccess().getRightParenthesisKeyword_0_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrimaryExpr__Group_0__2__Impl"


    // $ANTLR start "rule__Model__ExprAssignment"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:645:1: rule__Model__ExprAssignment : ( ruleExpression ) ;
    public final void rule__Model__ExprAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:649:1: ( ( ruleExpression ) )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:650:1: ( ruleExpression )
            {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:650:1: ( ruleExpression )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:651:1: ruleExpression
            {
             before(grammarAccess.getModelAccess().getExprExpressionParserRuleCall_0()); 
            pushFollow(FOLLOW_ruleExpression_in_rule__Model__ExprAssignment1260);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getModelAccess().getExprExpressionParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__ExprAssignment"


    // $ANTLR start "rule__Expression__VarAssignment_0"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:660:1: rule__Expression__VarAssignment_0 : ( RULE_ID ) ;
    public final void rule__Expression__VarAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:664:1: ( ( RULE_ID ) )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:665:1: ( RULE_ID )
            {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:665:1: ( RULE_ID )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:666:1: RULE_ID
            {
             before(grammarAccess.getExpressionAccess().getVarIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Expression__VarAssignment_01291); 
             after(grammarAccess.getExpressionAccess().getVarIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__VarAssignment_0"


    // $ANTLR start "rule__Expression__ExprAssignment_2"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:675:1: rule__Expression__ExprAssignment_2 : ( ruleAbstactArithmeticExpr ) ;
    public final void rule__Expression__ExprAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:679:1: ( ( ruleAbstactArithmeticExpr ) )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:680:1: ( ruleAbstactArithmeticExpr )
            {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:680:1: ( ruleAbstactArithmeticExpr )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:681:1: ruleAbstactArithmeticExpr
            {
             before(grammarAccess.getExpressionAccess().getExprAbstactArithmeticExprParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleAbstactArithmeticExpr_in_rule__Expression__ExprAssignment_21322);
            ruleAbstactArithmeticExpr();

            state._fsp--;

             after(grammarAccess.getExpressionAccess().getExprAbstactArithmeticExprParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Expression__ExprAssignment_2"


    // $ANTLR start "rule__ArithmeticExpr__RightAssignment_1_2"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:690:1: rule__ArithmeticExpr__RightAssignment_1_2 : ( rulePrimaryExpr ) ;
    public final void rule__ArithmeticExpr__RightAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:694:1: ( ( rulePrimaryExpr ) )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:695:1: ( rulePrimaryExpr )
            {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:695:1: ( rulePrimaryExpr )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:696:1: rulePrimaryExpr
            {
             before(grammarAccess.getArithmeticExprAccess().getRightPrimaryExprParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_rulePrimaryExpr_in_rule__ArithmeticExpr__RightAssignment_1_21353);
            rulePrimaryExpr();

            state._fsp--;

             after(grammarAccess.getArithmeticExprAccess().getRightPrimaryExprParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArithmeticExpr__RightAssignment_1_2"


    // $ANTLR start "rule__PrimaryExpr__ValueAssignment_1"
    // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:705:1: rule__PrimaryExpr__ValueAssignment_1 : ( RULE_INT ) ;
    public final void rule__PrimaryExpr__ValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:709:1: ( ( RULE_INT ) )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:710:1: ( RULE_INT )
            {
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:710:1: ( RULE_INT )
            // ../es.deusto.eghost.example6.mydsl.ui/src-gen/es/deusto/eghost/example6/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:711:1: RULE_INT
            {
             before(grammarAccess.getPrimaryExprAccess().getValueINTTerminalRuleCall_1_0()); 
            match(input,RULE_INT,FOLLOW_RULE_INT_in_rule__PrimaryExpr__ValueAssignment_11384); 
             after(grammarAccess.getPrimaryExprAccess().getValueINTTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrimaryExpr__ValueAssignment_1"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleModel_in_entryRuleModel61 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleModel68 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Model__ExprAssignment_in_ruleModel94 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_ruleExpression_in_entryRuleExpression122 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleExpression129 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Expression__Group__0_in_ruleExpression155 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAbstactArithmeticExpr_in_entryRuleAbstactArithmeticExpr182 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAbstactArithmeticExpr189 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleArithmeticExpr_in_ruleAbstactArithmeticExpr215 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleArithmeticExpr_in_entryRuleArithmeticExpr241 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleArithmeticExpr248 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ArithmeticExpr__Group__0_in_ruleArithmeticExpr274 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePrimaryExpr_in_entryRulePrimaryExpr301 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePrimaryExpr308 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PrimaryExpr__Alternatives_in_rulePrimaryExpr334 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_rule__ArithmeticExpr__Alternatives_1_1371 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__ArithmeticExpr__Alternatives_1_1391 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__ArithmeticExpr__Alternatives_1_1411 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rule__ArithmeticExpr__Alternatives_1_1431 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PrimaryExpr__Group_0__0_in_rule__PrimaryExpr__Alternatives465 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PrimaryExpr__ValueAssignment_1_in_rule__PrimaryExpr__Alternatives483 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Expression__Group__0__Impl_in_rule__Expression__Group__0514 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_rule__Expression__Group__1_in_rule__Expression__Group__0517 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Expression__VarAssignment_0_in_rule__Expression__Group__0__Impl544 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Expression__Group__1__Impl_in_rule__Expression__Group__1574 = new BitSet(new long[]{0x0000000000030020L});
    public static final BitSet FOLLOW_rule__Expression__Group__2_in_rule__Expression__Group__1577 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__Expression__Group__1__Impl605 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Expression__Group__2__Impl_in_rule__Expression__Group__2636 = new BitSet(new long[]{0x0000000000030020L});
    public static final BitSet FOLLOW_rule__Expression__Group__3_in_rule__Expression__Group__2639 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Expression__ExprAssignment_2_in_rule__Expression__Group__2__Impl666 = new BitSet(new long[]{0x0000000000020022L});
    public static final BitSet FOLLOW_rule__Expression__Group__3__Impl_in_rule__Expression__Group__3697 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__Expression__Group__3__Impl725 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ArithmeticExpr__Group__0__Impl_in_rule__ArithmeticExpr__Group__0764 = new BitSet(new long[]{0x0000000000007800L});
    public static final BitSet FOLLOW_rule__ArithmeticExpr__Group__1_in_rule__ArithmeticExpr__Group__0767 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePrimaryExpr_in_rule__ArithmeticExpr__Group__0__Impl794 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ArithmeticExpr__Group__1__Impl_in_rule__ArithmeticExpr__Group__1823 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ArithmeticExpr__Group_1__0_in_rule__ArithmeticExpr__Group__1__Impl850 = new BitSet(new long[]{0x0000000000007802L});
    public static final BitSet FOLLOW_rule__ArithmeticExpr__Group_1__0__Impl_in_rule__ArithmeticExpr__Group_1__0885 = new BitSet(new long[]{0x0000000000007800L});
    public static final BitSet FOLLOW_rule__ArithmeticExpr__Group_1__1_in_rule__ArithmeticExpr__Group_1__0888 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ArithmeticExpr__Group_1__1__Impl_in_rule__ArithmeticExpr__Group_1__1946 = new BitSet(new long[]{0x0000000000020020L});
    public static final BitSet FOLLOW_rule__ArithmeticExpr__Group_1__2_in_rule__ArithmeticExpr__Group_1__1949 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ArithmeticExpr__Alternatives_1_1_in_rule__ArithmeticExpr__Group_1__1__Impl976 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ArithmeticExpr__Group_1__2__Impl_in_rule__ArithmeticExpr__Group_1__21006 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ArithmeticExpr__RightAssignment_1_2_in_rule__ArithmeticExpr__Group_1__2__Impl1033 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PrimaryExpr__Group_0__0__Impl_in_rule__PrimaryExpr__Group_0__01069 = new BitSet(new long[]{0x0000000000020020L});
    public static final BitSet FOLLOW_rule__PrimaryExpr__Group_0__1_in_rule__PrimaryExpr__Group_0__01072 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__PrimaryExpr__Group_0__0__Impl1100 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PrimaryExpr__Group_0__1__Impl_in_rule__PrimaryExpr__Group_0__11131 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_rule__PrimaryExpr__Group_0__2_in_rule__PrimaryExpr__Group_0__11134 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleArithmeticExpr_in_rule__PrimaryExpr__Group_0__1__Impl1161 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PrimaryExpr__Group_0__2__Impl_in_rule__PrimaryExpr__Group_0__21190 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__PrimaryExpr__Group_0__2__Impl1218 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_rule__Model__ExprAssignment1260 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Expression__VarAssignment_01291 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAbstactArithmeticExpr_in_rule__Expression__ExprAssignment_21322 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePrimaryExpr_in_rule__ArithmeticExpr__RightAssignment_1_21353 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_rule__PrimaryExpr__ValueAssignment_11384 = new BitSet(new long[]{0x0000000000000002L});

}