package filesystem.templates;

import filesystem.*;;

public class Filesystem2Script
{
  protected static String nl;
  public static synchronized Filesystem2Script create(String lineSeparator)
  {
    nl = lineSeparator;
    Filesystem2Script result = new Filesystem2Script();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "#!/bin/bash";
  protected final String TEXT_2 = NL + "\t";
  protected final String TEXT_3 = NL + "\t\t";
  protected final String TEXT_4 = NL;
  protected final String TEXT_5 = NL + "\t\t";
  protected final String TEXT_6 = NL + "touch ";
  protected final String TEXT_7 = NL + "\t\t";
  protected final String TEXT_8 = NL + "\t";
  protected final String TEXT_9 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     Filesystem filesystem = (Filesystem) argument; 
    stringBuffer.append(TEXT_1);
     for(Drive drive: filesystem.getDrives()){ 
    stringBuffer.append(TEXT_2);
     for(File file: drive.getContents()){ 
    stringBuffer.append(TEXT_3);
     if(file instanceof Drive ||
			  file instanceof Folder){ 
    stringBuffer.append(TEXT_4);
    stringBuffer.append( new Folder2Script().generate(file) );
    stringBuffer.append(TEXT_5);
     } else if(file instanceof File &&
					 !(file instanceof Sync)){ 
    stringBuffer.append(TEXT_6);
    stringBuffer.append( file.getName() );
    stringBuffer.append(TEXT_7);
     } 
    stringBuffer.append(TEXT_8);
     } 
     } 
    stringBuffer.append(TEXT_9);
    return stringBuffer.toString();
  }
}
