/**
 */
package es.deusto.eghost.example6.mydsl.myDsl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstact Arithmetic Expr</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.deusto.eghost.example6.mydsl.myDsl.AbstactArithmeticExpr#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.deusto.eghost.example6.mydsl.myDsl.MyDslPackage#getAbstactArithmeticExpr()
 * @model
 * @generated
 */
public interface AbstactArithmeticExpr extends EObject
{
  /**
   * Returns the value of the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' attribute.
   * @see #setValue(int)
   * @see es.deusto.eghost.example6.mydsl.myDsl.MyDslPackage#getAbstactArithmeticExpr_Value()
   * @model
   * @generated
   */
  int getValue();

  /**
   * Sets the value of the '{@link es.deusto.eghost.example6.mydsl.myDsl.AbstactArithmeticExpr#getValue <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' attribute.
   * @see #getValue()
   * @generated
   */
  void setValue(int value);

} // AbstactArithmeticExpr
