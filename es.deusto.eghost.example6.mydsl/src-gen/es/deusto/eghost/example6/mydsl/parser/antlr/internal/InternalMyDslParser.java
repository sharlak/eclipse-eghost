package es.deusto.eghost.example6.mydsl.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import es.deusto.eghost.example6.mydsl.services.MyDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMyDslParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'='", "';'", "'+'", "'-'", "'*'", "'/'", "'('", "')'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_STRING=6;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=5;
    public static final int RULE_WS=9;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;

    // delegates
    // delegators


        public InternalMyDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMyDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMyDslParser.tokenNames; }
    public String getGrammarFileName() { return "../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g"; }



     	private MyDslGrammarAccess grammarAccess;
     	
        public InternalMyDslParser(TokenStream input, MyDslGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Model";	
       	}
       	
       	@Override
       	protected MyDslGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleModel"
    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:67:1: entryRuleModel returns [EObject current=null] : iv_ruleModel= ruleModel EOF ;
    public final EObject entryRuleModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModel = null;


        try {
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:68:2: (iv_ruleModel= ruleModel EOF )
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:69:2: iv_ruleModel= ruleModel EOF
            {
             newCompositeNode(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_ruleModel_in_entryRuleModel75);
            iv_ruleModel=ruleModel();

            state._fsp--;

             current =iv_ruleModel; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleModel85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:76:1: ruleModel returns [EObject current=null] : ( (lv_expr_0_0= ruleExpression ) )* ;
    public final EObject ruleModel() throws RecognitionException {
        EObject current = null;

        EObject lv_expr_0_0 = null;


         enterRule(); 
            
        try {
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:79:28: ( ( (lv_expr_0_0= ruleExpression ) )* )
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:80:1: ( (lv_expr_0_0= ruleExpression ) )*
            {
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:80:1: ( (lv_expr_0_0= ruleExpression ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==RULE_ID) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:81:1: (lv_expr_0_0= ruleExpression )
            	    {
            	    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:81:1: (lv_expr_0_0= ruleExpression )
            	    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:82:3: lv_expr_0_0= ruleExpression
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getModelAccess().getExprExpressionParserRuleCall_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleExpression_in_ruleModel130);
            	    lv_expr_0_0=ruleExpression();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getModelRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"expr",
            	            		lv_expr_0_0, 
            	            		"Expression");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleExpression"
    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:106:1: entryRuleExpression returns [EObject current=null] : iv_ruleExpression= ruleExpression EOF ;
    public final EObject entryRuleExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpression = null;


        try {
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:107:2: (iv_ruleExpression= ruleExpression EOF )
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:108:2: iv_ruleExpression= ruleExpression EOF
            {
             newCompositeNode(grammarAccess.getExpressionRule()); 
            pushFollow(FOLLOW_ruleExpression_in_entryRuleExpression166);
            iv_ruleExpression=ruleExpression();

            state._fsp--;

             current =iv_ruleExpression; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleExpression176); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:115:1: ruleExpression returns [EObject current=null] : ( ( (lv_var_0_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expr_2_0= ruleAbstactArithmeticExpr ) )* otherlv_3= ';' ) ;
    public final EObject ruleExpression() throws RecognitionException {
        EObject current = null;

        Token lv_var_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_expr_2_0 = null;


         enterRule(); 
            
        try {
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:118:28: ( ( ( (lv_var_0_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expr_2_0= ruleAbstactArithmeticExpr ) )* otherlv_3= ';' ) )
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:119:1: ( ( (lv_var_0_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expr_2_0= ruleAbstactArithmeticExpr ) )* otherlv_3= ';' )
            {
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:119:1: ( ( (lv_var_0_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expr_2_0= ruleAbstactArithmeticExpr ) )* otherlv_3= ';' )
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:119:2: ( (lv_var_0_0= RULE_ID ) ) otherlv_1= '=' ( (lv_expr_2_0= ruleAbstactArithmeticExpr ) )* otherlv_3= ';'
            {
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:119:2: ( (lv_var_0_0= RULE_ID ) )
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:120:1: (lv_var_0_0= RULE_ID )
            {
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:120:1: (lv_var_0_0= RULE_ID )
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:121:3: lv_var_0_0= RULE_ID
            {
            lv_var_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleExpression218); 

            			newLeafNode(lv_var_0_0, grammarAccess.getExpressionAccess().getVarIDTerminalRuleCall_0_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getExpressionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"var",
                    		lv_var_0_0, 
                    		"ID");
            	    

            }


            }

            otherlv_1=(Token)match(input,11,FOLLOW_11_in_ruleExpression235); 

                	newLeafNode(otherlv_1, grammarAccess.getExpressionAccess().getEqualsSignKeyword_1());
                
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:141:1: ( (lv_expr_2_0= ruleAbstactArithmeticExpr ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_INT||LA2_0==17) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:142:1: (lv_expr_2_0= ruleAbstactArithmeticExpr )
            	    {
            	    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:142:1: (lv_expr_2_0= ruleAbstactArithmeticExpr )
            	    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:143:3: lv_expr_2_0= ruleAbstactArithmeticExpr
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getExpressionAccess().getExprAbstactArithmeticExprParserRuleCall_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleAbstactArithmeticExpr_in_ruleExpression256);
            	    lv_expr_2_0=ruleAbstactArithmeticExpr();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getExpressionRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"expr",
            	            		lv_expr_2_0, 
            	            		"AbstactArithmeticExpr");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            otherlv_3=(Token)match(input,12,FOLLOW_12_in_ruleExpression269); 

                	newLeafNode(otherlv_3, grammarAccess.getExpressionAccess().getSemicolonKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleAbstactArithmeticExpr"
    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:171:1: entryRuleAbstactArithmeticExpr returns [EObject current=null] : iv_ruleAbstactArithmeticExpr= ruleAbstactArithmeticExpr EOF ;
    public final EObject entryRuleAbstactArithmeticExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAbstactArithmeticExpr = null;


        try {
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:172:2: (iv_ruleAbstactArithmeticExpr= ruleAbstactArithmeticExpr EOF )
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:173:2: iv_ruleAbstactArithmeticExpr= ruleAbstactArithmeticExpr EOF
            {
             newCompositeNode(grammarAccess.getAbstactArithmeticExprRule()); 
            pushFollow(FOLLOW_ruleAbstactArithmeticExpr_in_entryRuleAbstactArithmeticExpr305);
            iv_ruleAbstactArithmeticExpr=ruleAbstactArithmeticExpr();

            state._fsp--;

             current =iv_ruleAbstactArithmeticExpr; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleAbstactArithmeticExpr315); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAbstactArithmeticExpr"


    // $ANTLR start "ruleAbstactArithmeticExpr"
    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:180:1: ruleAbstactArithmeticExpr returns [EObject current=null] : this_ArithmeticExpr_0= ruleArithmeticExpr ;
    public final EObject ruleAbstactArithmeticExpr() throws RecognitionException {
        EObject current = null;

        EObject this_ArithmeticExpr_0 = null;


         enterRule(); 
            
        try {
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:183:28: (this_ArithmeticExpr_0= ruleArithmeticExpr )
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:185:5: this_ArithmeticExpr_0= ruleArithmeticExpr
            {
             
                    newCompositeNode(grammarAccess.getAbstactArithmeticExprAccess().getArithmeticExprParserRuleCall()); 
                
            pushFollow(FOLLOW_ruleArithmeticExpr_in_ruleAbstactArithmeticExpr361);
            this_ArithmeticExpr_0=ruleArithmeticExpr();

            state._fsp--;

             
                    current = this_ArithmeticExpr_0; 
                    afterParserOrEnumRuleCall();
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAbstactArithmeticExpr"


    // $ANTLR start "entryRuleArithmeticExpr"
    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:201:1: entryRuleArithmeticExpr returns [EObject current=null] : iv_ruleArithmeticExpr= ruleArithmeticExpr EOF ;
    public final EObject entryRuleArithmeticExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleArithmeticExpr = null;


        try {
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:202:2: (iv_ruleArithmeticExpr= ruleArithmeticExpr EOF )
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:203:2: iv_ruleArithmeticExpr= ruleArithmeticExpr EOF
            {
             newCompositeNode(grammarAccess.getArithmeticExprRule()); 
            pushFollow(FOLLOW_ruleArithmeticExpr_in_entryRuleArithmeticExpr395);
            iv_ruleArithmeticExpr=ruleArithmeticExpr();

            state._fsp--;

             current =iv_ruleArithmeticExpr; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleArithmeticExpr405); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleArithmeticExpr"


    // $ANTLR start "ruleArithmeticExpr"
    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:210:1: ruleArithmeticExpr returns [EObject current=null] : (this_PrimaryExpr_0= rulePrimaryExpr ( () (otherlv_2= '+' | otherlv_3= '-' | otherlv_4= '*' | otherlv_5= '/' ) ( (lv_right_6_0= rulePrimaryExpr ) ) )* ) ;
    public final EObject ruleArithmeticExpr() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        EObject this_PrimaryExpr_0 = null;

        EObject lv_right_6_0 = null;


         enterRule(); 
            
        try {
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:213:28: ( (this_PrimaryExpr_0= rulePrimaryExpr ( () (otherlv_2= '+' | otherlv_3= '-' | otherlv_4= '*' | otherlv_5= '/' ) ( (lv_right_6_0= rulePrimaryExpr ) ) )* ) )
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:214:1: (this_PrimaryExpr_0= rulePrimaryExpr ( () (otherlv_2= '+' | otherlv_3= '-' | otherlv_4= '*' | otherlv_5= '/' ) ( (lv_right_6_0= rulePrimaryExpr ) ) )* )
            {
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:214:1: (this_PrimaryExpr_0= rulePrimaryExpr ( () (otherlv_2= '+' | otherlv_3= '-' | otherlv_4= '*' | otherlv_5= '/' ) ( (lv_right_6_0= rulePrimaryExpr ) ) )* )
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:215:5: this_PrimaryExpr_0= rulePrimaryExpr ( () (otherlv_2= '+' | otherlv_3= '-' | otherlv_4= '*' | otherlv_5= '/' ) ( (lv_right_6_0= rulePrimaryExpr ) ) )*
            {
             
                    newCompositeNode(grammarAccess.getArithmeticExprAccess().getPrimaryExprParserRuleCall_0()); 
                
            pushFollow(FOLLOW_rulePrimaryExpr_in_ruleArithmeticExpr452);
            this_PrimaryExpr_0=rulePrimaryExpr();

            state._fsp--;

             
                    current = this_PrimaryExpr_0; 
                    afterParserOrEnumRuleCall();
                
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:223:1: ( () (otherlv_2= '+' | otherlv_3= '-' | otherlv_4= '*' | otherlv_5= '/' ) ( (lv_right_6_0= rulePrimaryExpr ) ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( ((LA4_0>=13 && LA4_0<=16)) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:223:2: () (otherlv_2= '+' | otherlv_3= '-' | otherlv_4= '*' | otherlv_5= '/' ) ( (lv_right_6_0= rulePrimaryExpr ) )
            	    {
            	    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:223:2: ()
            	    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:224:5: 
            	    {

            	            current = forceCreateModelElementAndSet(
            	                grammarAccess.getArithmeticExprAccess().getOpLeftAction_1_0(),
            	                current);
            	        

            	    }

            	    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:229:2: (otherlv_2= '+' | otherlv_3= '-' | otherlv_4= '*' | otherlv_5= '/' )
            	    int alt3=4;
            	    switch ( input.LA(1) ) {
            	    case 13:
            	        {
            	        alt3=1;
            	        }
            	        break;
            	    case 14:
            	        {
            	        alt3=2;
            	        }
            	        break;
            	    case 15:
            	        {
            	        alt3=3;
            	        }
            	        break;
            	    case 16:
            	        {
            	        alt3=4;
            	        }
            	        break;
            	    default:
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 3, 0, input);

            	        throw nvae;
            	    }

            	    switch (alt3) {
            	        case 1 :
            	            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:229:4: otherlv_2= '+'
            	            {
            	            otherlv_2=(Token)match(input,13,FOLLOW_13_in_ruleArithmeticExpr474); 

            	                	newLeafNode(otherlv_2, grammarAccess.getArithmeticExprAccess().getPlusSignKeyword_1_1_0());
            	                

            	            }
            	            break;
            	        case 2 :
            	            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:234:7: otherlv_3= '-'
            	            {
            	            otherlv_3=(Token)match(input,14,FOLLOW_14_in_ruleArithmeticExpr492); 

            	                	newLeafNode(otherlv_3, grammarAccess.getArithmeticExprAccess().getHyphenMinusKeyword_1_1_1());
            	                

            	            }
            	            break;
            	        case 3 :
            	            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:239:7: otherlv_4= '*'
            	            {
            	            otherlv_4=(Token)match(input,15,FOLLOW_15_in_ruleArithmeticExpr510); 

            	                	newLeafNode(otherlv_4, grammarAccess.getArithmeticExprAccess().getAsteriskKeyword_1_1_2());
            	                

            	            }
            	            break;
            	        case 4 :
            	            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:244:7: otherlv_5= '/'
            	            {
            	            otherlv_5=(Token)match(input,16,FOLLOW_16_in_ruleArithmeticExpr528); 

            	                	newLeafNode(otherlv_5, grammarAccess.getArithmeticExprAccess().getSolidusKeyword_1_1_3());
            	                

            	            }
            	            break;

            	    }

            	    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:248:2: ( (lv_right_6_0= rulePrimaryExpr ) )
            	    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:249:1: (lv_right_6_0= rulePrimaryExpr )
            	    {
            	    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:249:1: (lv_right_6_0= rulePrimaryExpr )
            	    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:250:3: lv_right_6_0= rulePrimaryExpr
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getArithmeticExprAccess().getRightPrimaryExprParserRuleCall_1_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_rulePrimaryExpr_in_ruleArithmeticExpr550);
            	    lv_right_6_0=rulePrimaryExpr();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getArithmeticExprRule());
            	    	        }
            	           		set(
            	           			current, 
            	           			"right",
            	            		lv_right_6_0, 
            	            		"PrimaryExpr");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleArithmeticExpr"


    // $ANTLR start "entryRulePrimaryExpr"
    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:274:1: entryRulePrimaryExpr returns [EObject current=null] : iv_rulePrimaryExpr= rulePrimaryExpr EOF ;
    public final EObject entryRulePrimaryExpr() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimaryExpr = null;


        try {
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:275:2: (iv_rulePrimaryExpr= rulePrimaryExpr EOF )
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:276:2: iv_rulePrimaryExpr= rulePrimaryExpr EOF
            {
             newCompositeNode(grammarAccess.getPrimaryExprRule()); 
            pushFollow(FOLLOW_rulePrimaryExpr_in_entryRulePrimaryExpr588);
            iv_rulePrimaryExpr=rulePrimaryExpr();

            state._fsp--;

             current =iv_rulePrimaryExpr; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePrimaryExpr598); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimaryExpr"


    // $ANTLR start "rulePrimaryExpr"
    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:283:1: rulePrimaryExpr returns [EObject current=null] : ( (otherlv_0= '(' this_ArithmeticExpr_1= ruleArithmeticExpr otherlv_2= ')' ) | ( (lv_value_3_0= RULE_INT ) ) ) ;
    public final EObject rulePrimaryExpr() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token lv_value_3_0=null;
        EObject this_ArithmeticExpr_1 = null;


         enterRule(); 
            
        try {
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:286:28: ( ( (otherlv_0= '(' this_ArithmeticExpr_1= ruleArithmeticExpr otherlv_2= ')' ) | ( (lv_value_3_0= RULE_INT ) ) ) )
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:287:1: ( (otherlv_0= '(' this_ArithmeticExpr_1= ruleArithmeticExpr otherlv_2= ')' ) | ( (lv_value_3_0= RULE_INT ) ) )
            {
            // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:287:1: ( (otherlv_0= '(' this_ArithmeticExpr_1= ruleArithmeticExpr otherlv_2= ')' ) | ( (lv_value_3_0= RULE_INT ) ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==17) ) {
                alt5=1;
            }
            else if ( (LA5_0==RULE_INT) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:287:2: (otherlv_0= '(' this_ArithmeticExpr_1= ruleArithmeticExpr otherlv_2= ')' )
                    {
                    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:287:2: (otherlv_0= '(' this_ArithmeticExpr_1= ruleArithmeticExpr otherlv_2= ')' )
                    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:287:4: otherlv_0= '(' this_ArithmeticExpr_1= ruleArithmeticExpr otherlv_2= ')'
                    {
                    otherlv_0=(Token)match(input,17,FOLLOW_17_in_rulePrimaryExpr636); 

                        	newLeafNode(otherlv_0, grammarAccess.getPrimaryExprAccess().getLeftParenthesisKeyword_0_0());
                        
                     
                            newCompositeNode(grammarAccess.getPrimaryExprAccess().getArithmeticExprParserRuleCall_0_1()); 
                        
                    pushFollow(FOLLOW_ruleArithmeticExpr_in_rulePrimaryExpr658);
                    this_ArithmeticExpr_1=ruleArithmeticExpr();

                    state._fsp--;

                     
                            current = this_ArithmeticExpr_1; 
                            afterParserOrEnumRuleCall();
                        
                    otherlv_2=(Token)match(input,18,FOLLOW_18_in_rulePrimaryExpr669); 

                        	newLeafNode(otherlv_2, grammarAccess.getPrimaryExprAccess().getRightParenthesisKeyword_0_2());
                        

                    }


                    }
                    break;
                case 2 :
                    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:305:6: ( (lv_value_3_0= RULE_INT ) )
                    {
                    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:305:6: ( (lv_value_3_0= RULE_INT ) )
                    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:306:1: (lv_value_3_0= RULE_INT )
                    {
                    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:306:1: (lv_value_3_0= RULE_INT )
                    // ../es.deusto.eghost.example6.mydsl/src-gen/es/deusto/eghost/example6/mydsl/parser/antlr/internal/InternalMyDsl.g:307:3: lv_value_3_0= RULE_INT
                    {
                    lv_value_3_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_rulePrimaryExpr693); 

                    			newLeafNode(lv_value_3_0, grammarAccess.getPrimaryExprAccess().getValueINTTerminalRuleCall_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getPrimaryExprRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"value",
                            		lv_value_3_0, 
                            		"INT");
                    	    

                    }


                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimaryExpr"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleModel_in_entryRuleModel75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleModel85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleExpression_in_ruleModel130 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_ruleExpression_in_entryRuleExpression166 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleExpression176 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleExpression218 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_ruleExpression235 = new BitSet(new long[]{0x0000000000021020L});
    public static final BitSet FOLLOW_ruleAbstactArithmeticExpr_in_ruleExpression256 = new BitSet(new long[]{0x0000000000021020L});
    public static final BitSet FOLLOW_12_in_ruleExpression269 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAbstactArithmeticExpr_in_entryRuleAbstactArithmeticExpr305 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAbstactArithmeticExpr315 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleArithmeticExpr_in_ruleAbstactArithmeticExpr361 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleArithmeticExpr_in_entryRuleArithmeticExpr395 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleArithmeticExpr405 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePrimaryExpr_in_ruleArithmeticExpr452 = new BitSet(new long[]{0x000000000001E002L});
    public static final BitSet FOLLOW_13_in_ruleArithmeticExpr474 = new BitSet(new long[]{0x0000000000020020L});
    public static final BitSet FOLLOW_14_in_ruleArithmeticExpr492 = new BitSet(new long[]{0x0000000000020020L});
    public static final BitSet FOLLOW_15_in_ruleArithmeticExpr510 = new BitSet(new long[]{0x0000000000020020L});
    public static final BitSet FOLLOW_16_in_ruleArithmeticExpr528 = new BitSet(new long[]{0x0000000000020020L});
    public static final BitSet FOLLOW_rulePrimaryExpr_in_ruleArithmeticExpr550 = new BitSet(new long[]{0x000000000001E002L});
    public static final BitSet FOLLOW_rulePrimaryExpr_in_entryRulePrimaryExpr588 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePrimaryExpr598 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rulePrimaryExpr636 = new BitSet(new long[]{0x0000000000020020L});
    public static final BitSet FOLLOW_ruleArithmeticExpr_in_rulePrimaryExpr658 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_rulePrimaryExpr669 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_rulePrimaryExpr693 = new BitSet(new long[]{0x0000000000000002L});

}