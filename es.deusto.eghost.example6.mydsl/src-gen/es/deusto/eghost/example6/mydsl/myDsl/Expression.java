/**
 */
package es.deusto.eghost.example6.mydsl.myDsl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.deusto.eghost.example6.mydsl.myDsl.Expression#getVar <em>Var</em>}</li>
 *   <li>{@link es.deusto.eghost.example6.mydsl.myDsl.Expression#getExpr <em>Expr</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.deusto.eghost.example6.mydsl.myDsl.MyDslPackage#getExpression()
 * @model
 * @generated
 */
public interface Expression extends EObject
{
  /**
   * Returns the value of the '<em><b>Var</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Var</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Var</em>' attribute.
   * @see #setVar(String)
   * @see es.deusto.eghost.example6.mydsl.myDsl.MyDslPackage#getExpression_Var()
   * @model
   * @generated
   */
  String getVar();

  /**
   * Sets the value of the '{@link es.deusto.eghost.example6.mydsl.myDsl.Expression#getVar <em>Var</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Var</em>' attribute.
   * @see #getVar()
   * @generated
   */
  void setVar(String value);

  /**
   * Returns the value of the '<em><b>Expr</b></em>' containment reference list.
   * The list contents are of type {@link es.deusto.eghost.example6.mydsl.myDsl.AbstactArithmeticExpr}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Expr</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Expr</em>' containment reference list.
   * @see es.deusto.eghost.example6.mydsl.myDsl.MyDslPackage#getExpression_Expr()
   * @model containment="true"
   * @generated
   */
  EList<AbstactArithmeticExpr> getExpr();

} // Expression
